* [1790 Census Data By County](1790/1790-number-of-persons.pdf) : Downloaded 2024-03-24 from [census.gov](https://www2.census.gov/library/publications/decennial/1790/number-of-persons.pdf)
* [1800 Census Data By County](1800/1800-numbers.pdf) : Downloaded 2024-03-24 from [census.gov](https://www2.census.gov/library/publications/decennial/1800/1800-returns.pdf)
* 1810 Census Data By County
* [1820 Census Data By County](1820/1820a-02.pdf) : Downloaded 2024-03-24 from [census.gov](https://www2.census.gov/library/publications/decennial/1820/1820a.zip)
* 1830 Census Data By County
* 1840 Census Data By County
* [1850 Census Data By County](1850/1850a-14.pdf) : Downloaded 2024-03-24 from [census.gov](https://www2.census.gov/library/publications/decennial/1850/1850a/1850a-14.pdf)
* [1860 Census Data by State/Territory and County](1860/retrieve.sh) : Downloaded 2024-03-30 from [census.gov](https://www.census.gov/library/publications/1864/dec/1860a.html). Used a script due to multiple PDFs being required.
* [1870 Census Data By State/Territory and County](1870/retrieve.sh) : Downloaded 2024-03-30 from [census.gov](https://www.census.gov/library/publications/1872/dec/1870a.html). Used a script due to multiple PDFs being required.
* [1880 Census Data By County](1880/1880_v1-08.pdf) : Downloaded 2024-03-30 from [census.gov](https://www.census.gov/library/publications/1883/dec/vol-01-population.html)
* [1890 Census Data By County](1890/1890a_v1-06.pdf) : Downloaded 2024-03-30 from [census.gov](https://www.census.gov/library/publications/1895/dec/volume-1.html)
* [1900 Census Data By County](1900/volume-1-p2.pdf): Downloaeed 2024-03-30 from [census.gov](https://www.census.gov/library/publications/1901/dec/vol-01-population.html)
* [1910 Census Data By County](1910/volume-1-p3.pdf) : Downloaded 2024-03-30 from [census.gov](https://www.census.gov/library/publications/1913/dec/vol-1-population.html)
* [1920 Census Data By County](1920/retrieve.sh) : Downloaded 2024-03-30 from [census.gov](https://www.census.gov/library/publications/1921/dec/vol-01-population.html)
* [1930 Census Data By County](1930/retrieve.sh) : Downloaded 2024-04-06 from [census.gov](https://www.census.gov/programs-surveys/decennial-census/decade/decennial-publications.1930.html)
* [1940 Census Data By County](1940/retrieve.sh) : Downloaded 2024-04-06 from [census.gov](https://www.census.gov/library/publications/1942/dec/population-vol-1.html)
* [1950 Census Data By County](1950/retrieve.sh) : Downloaded 2024-04-06 from [census.gov](https://www.census.gov/library/publications/1953/dec/housing-vol-01.html)
* [1960 Census Data By County](1960/retrieve.sh) : Downloaded 2024-04-06 from [census.gov](https://www.census.gov/library/publications/1960/dec/population-pc-a1.html)
* 